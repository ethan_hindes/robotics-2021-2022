#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import time
import math

#it is always assumed that the arm starts in a straight line
#the motors have no global notion of angle

#left is positive
targetx =10
targety = 15
tolerance = 1
time_step = 0.1
speed = 25
ev3 = EV3Brick()
shoulder = Motor(Port.D)
elbow = Motor(Port.C)
elbow.reset_angle(0)
shoulder.reset_angle(0)
def y(q1,q2):
    return 13 * math.cos(math.radians(q1)) + 10.5 * math.cos( math.radians(q1 + q2 ))
def x(q1,q2):
    return 13 * math.sin(math.radians(q1)) + 10.5 * math.sin( math.radians(q1 + q2 ))
def _error():
    return (targetx-x(shoulder.angle(), elbow.angle()))**2+(targety-y(shoulder.angle(),elbow.angle()))**2
def ccd():
    i = 0
    error = _error()
    while error>tolerance and i<50:
        #try running shoulder forward
        last_error = error
        shoulder.run(speed)
        time.sleep(time_step)
        shoulder.brake()
        error = _error()
        while error<last_error:
            last_error = error
            shoulder.run(speed)
            time.sleep(time_step)
            error = _error()
        shoulder.brake()
        #try running backward
        last_error = error
        shoulder.run(-1*speed)
        time.sleep(time_step)
        shoulder.brake()
        error = _error()
        while error<last_error:
            last_error = error
            shoulder.run(-1*speed)
            time.sleep(time_step)
            error = _error()
        shoulder.brake()
        #try running elbow forward
        last_error = error
        elbow.run(speed)
        time.sleep(time_step)
        elbow.brake()
        error = _error()
        while error<last_error:
            last_error = error
            elbow.run(speed)
            time.sleep(time_step)
            error = _error()
        elbow.brake()
        #try running backward
        last_error = error
        elbow.run(-1*speed)
        time.sleep(time_step)
        elbow.brake()
        error = _error()
        while error<last_error:
            last_error = error
            elbow.run(-1*speed)
            time.sleep(time_step)
            error = _error()
        elbow.brake()
        i+=1
ccd()



