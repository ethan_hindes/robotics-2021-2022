#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
from pybricks.ev3devices import Motor
from time import sleep
import math

SPEED = 300

# Create your objects here.
ev3 = EV3Brick()

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

def get_time(distance):
    return (360 * distance) / (5.6 * math.pi) / SPEED

def forwards(distance):
    time = get_time(distance)
    left_motor.run(SPEED)
    right_motor.run(SPEED)
    sleep(time)
    left_motor.stop()
    right_motor.stop()

def turn(direction, degrees):
    if direction == 'cc':
        left_motor.run_angle(SPEED, ANGLE, wait=False)
        right_motor.run_angle(SPEED, -ANGLE)
    elif direction == 'c':
        left_motor.run_angle(SPEED, -ANGLE, wait=False)
        right_motor.run_angle(SPEED, ANGLE)


left_motor.reset_angle(0)
right_motor.reset_angle(0)

forwards(5.6 * math.pi)

print('Left Motor Angle: {}, Right Motor Angle: {}'.format( left_motor.angle(), right_motor.angle() ) )