from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

from lab_math import *

class Pen:

    def __init__(self):
        self.lowered = False
        self.motor = Motor(Port.C)

    def up(self):
        if self.lowered:
            self.motor.run_target(100, 0)
            self.lowered = False

    def down(self):
        if not self.lowered:
            self.motor.run_target(100, 70)
            self.lowered = True

class Arm:

    def __init__(self):
        self.elbow = Motor(Port.B)
        self.shoulder = Motor(Port.A)

    def turn_elbow(self, speed, angle):
        """
        Used to turn the elbow joint to a given x, y coordinate at a given speed. Wraps movement
        with insurance measures to reduce error.

        @param speed: what speed to rotate at (deg/s)
        @param angle: desired angle to rotate to

        @returns: None
        """

        # Get angle of shoulder before motion
        pre = self.shoulder.angle()

        # Rotate the elbow to desired location
        self.elbow.run_target(speed, angle)
        self.elbow.stop()

        # Make sure the shoulder is still at its starging location
        self.shoulder.run_target(speed, pre)
        self.shoulder.stop()

    def turn_shoulder(self, speed, angle):
        """
        Used to turn the shoulder joint to a given x, y coordinate at a given speed. Wraps movement
        with insurance measures to reduce error.

        @param speed: what speed to rotate at (deg/s)
        @param angle: desired angle to rotate to

        @returns: None
        """

        # Get angle of elbow before motion
        pre = self.elbow.angle()

        # Rotate the shoulder to desired location
        self.shoulder.run_target(speed, angle)
        self.shoulder.stop()

        # Make sure the elbow is still at its starging location
        self.elbow.run_target(speed, pre)
        self.elbow.stop()

    def go_to(self, x, y, speed=75):

        angles = get_angles(x, y)
        # TODO verify angle options can work and pick one
        q2, q1 = angles['positive']
        q1 -= 90

        # Actually move to position
        self.turn_shoulder(speed, q1)
        self.turn_elbow(speed, q2)