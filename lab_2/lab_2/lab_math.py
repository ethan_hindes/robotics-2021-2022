import math

# Robot Geometric Constants
A1 = 13.0 # TODO Correct
A2 = 10.5 # TODO Correct

def get_angles(x, y):

    Q2 = math.acos( ( x**2 + y**2 - A1**2 - A2**2 ) / ( 2 * A1 * A2 ) )
    def get_q1(q2):
        return math.atan2( y, x ) - math.atan2( A2 * math.sin( q2 ), A1 + A2 * math.cos( q2 ) )
    Q1 = get_q1( Q2 )
    Q1_neg = get_q1( -1 * Q2 )

    return { 
        'positive': ( math.degrees( Q2 ), math.degrees( Q1 ) ),
        'negative': ( math.degrees( -1 * Q2 ), math.degrees( Q1_neg ) )
    }

def get_position(q1, q2):
    x = A1 * math.cos( q1 ) + A2 * math.cos( q1 + q2 )
    y = A1 * math.sin( q1 ) + A2 * math.sin( q1 + q2 )
    return (x, y)
