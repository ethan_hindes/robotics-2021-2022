#!/usr/bin/env pybricks-micropython

from arm_control import *

pen = Pen()
arm = Arm()

pen.down()
arm.go_to(10,20)
pen.up()
arm.go_to(0, 23.5)